import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

import { resolve } from 'path';
import Inspect from 'vite-plugin-inspect';
import purgeCSS from 'rollup-plugin-purgecss';
// import createImportPlugin from "vite-plugin-import";

var currDirResolve = (path: string) => resolve(__dirname, path);

// https://vitejs.dev/config/
export default defineConfig({
    resolve: {
        alias: {
            '@public': currDirResolve('public'),
            '@src': currDirResolve('src'),
            '@components': currDirResolve('src/components'),
            '@containers': currDirResolve('src/containers'),
            '@contexts': currDirResolve('src/contexts'),
            '@framework': currDirResolve('src/framework/basic-rest'),
            '@settings': currDirResolve('src/settings'),
            '@styles': currDirResolve('src/styles'),
            '@utils': currDirResolve('src/utils')
        }
    },
    plugins: [],
    build: {
        sourcemap: true
    },
    server: {
        port: 3001
    }
});
