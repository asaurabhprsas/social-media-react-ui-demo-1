const ROUTES = {
  NOT_FOUND_ROUTE : { path: '/*', component: NotFound }
};

export default ROUTES;
