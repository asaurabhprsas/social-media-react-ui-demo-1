import React from 'react';
import ROUTES from '@src/router/routes';
import { Route, Routes } from 'react-router-dom';


const MainRouter = () => {
  return (
    <Routes>
      <Route path={HOME_ROUTE.path} element={<HOME_ROUTE.component />} />
      <Route path={NOT_FOUND_ROUTE.path} element={<NOT_FOUND_ROUTE.component />} />
    </Routes>
  );
};

export default MainRouter;
