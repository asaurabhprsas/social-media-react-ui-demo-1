
export const makeNestedRoutePath = (path: string) => path.replace(/^(\/)|(\/)$/g, "")
