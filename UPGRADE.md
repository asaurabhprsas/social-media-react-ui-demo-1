- Install Plugins

  > yarn add -D tailwindcss postcss autoprefixer purgecss rollup-plugin-purgecss sass vite-plugin-inspect

- update tsconfig.json
- update vite.config.js
- yarn tailwindcss init -p
- update content field of tailwind.config.js

- no utility detected fix
  > update content in tailwind.config.js to ./src/\*_/_.{html,js,jsx,ts,tsx}
- if warning about utitlity > ignore its because not using any class in file
